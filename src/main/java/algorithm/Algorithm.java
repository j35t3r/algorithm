package algorithm;

/**
 * Created by jester on 8/21/16.
 */
public abstract class Algorithm {
    protected static String name = null;
    public abstract void execute();
    public abstract String getName();
}
