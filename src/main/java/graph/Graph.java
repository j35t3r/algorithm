package graph;
import java.util.*;

public class Graph {
    private static String name;

    private static Map<String, Vertex> vertices;
    private static List<Edge> edges;
    private static int count_vertex = 0;
    private static int count_edge = 0;

    public Graph(String name) {
        this.name = name;
    }

    public String getName() { return this.name; }

    public void addVertex(Vertex v) {
        vertices.put(v.getVertex(), v);
    }

    public Vertex getVertex(String name) {
        if(vertices.containsKey(name)) {
            return vertices.get(name);
        } return null;
    }

    public void addEdge(Vertex v1, Vertex v2, int weight) {
        Edge edge = new Edge(v1, v2, this, Integer.toString(count_edge++), weight);
        edges.add(edge);
    }

    public boolean assertVertexExists(Vertex v) {
        if(vertices.containsValue(v)) {
            return true;
        } return false;
    }
}
