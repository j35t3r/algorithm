package graph;

/**
 * Created by jester on 8/21/16.
 */
public class Edge {
    private static String name;
    private static int weight;
    private Vertex v1;
    private Vertex v2;
    private Graph g;

    public Edge(Vertex v1, Vertex v2, Graph g, String name, int weight) {
        this.v1 = v1;
        this.v2 = v2;
        this.g = g;
        this.name = name;
        this.weight = weight;
    }
}
