package graph;

/**
 * Created by jester on 8/21/16.
 */
public class Vertex {
    private static String name;

    public Vertex(String name) {
        this.name = name;
    }

    public String getVertex() { return this.name; }
}
